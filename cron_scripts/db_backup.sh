#!/bin/bash
docker_postgres_container=$(docker ps -aqf "volume=alfa_pgdata")

docker exec -i $docker_postgres_container pg_dump -h localhost -U test -F c -f ../backup/test_$(date +"%d-%m-%Y-%H-%M").tar.gz test

# find ../backup -name "test*.gz" -mtime +7 -type f -delete
