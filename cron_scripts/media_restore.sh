#!/bin/bash
#export PASSPHRASE=SomeLongGeneratedHardToCrackKey

duplicity restore --no-encryption \
--archive-dir /mnt/fins_app_backup/duplicity/cache/ \
file:///mnt/fins_app_backup/media /home/prod_user/restored_media
