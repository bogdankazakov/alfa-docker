#!/bin/bash
docker_postgres_container=$(docker ps -aqf "volume=alfa_pgdata")
backup_url="../backup/"
#latest
file=$( find $backup_url -name "*.gz" -print0 | xargs -r -0 ls -1 -t | head -1 )

echo $file
# use -c to ovewrite
docker exec -i $docker_postgres_container \
pg_restore -c -h localhost -U test -F c -d test $file
#pg_restore -c -h localhost --no-owner --role=test -F c -d test $file


