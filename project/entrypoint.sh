#!/bin/sh

# First install Flag
FIRSTTIME=false
DIR=./vendor/tcg/voyager
if [ ! -d "$DIR" ]; then
    echo "$DIR don't exist, means it's a first launch"
    FIRSTTIME=true
fi

# Install dependencies
echo "Install dependencies"
composer install

# Giving permissions
echo "Giving permissions"
chown -R www-data:www-data /srv/www/project/storage
chown -R www-data:www-data /srv/www/project/bootstrap/cache
#chown -R www-data:www-data /srv/www/project/entrypoint.sh
#chmod 776 /srv/www/project/entrypoint.sh

# Apply database migrations
echo "Apply database migrations"
php artisan migrate


if [ $FIRSTTIME = true ]; then
    echo "Voyager scripts at first launch"
    # Voyager
    php artisan vendor:publish --provider="TCG\Voyager\VoyagerServiceProvider"
    php artisan vendor:publish --provider="Intervention\Image\ImageServiceProviderLaravelRecent"
    php artisan db:seed --class=VoyagerDatabaseSeeder
    php artisan hook:setup
    php artisan storage:link
    composer dump-autoload
    php artisan db:seed --class=VoyagerDummyDatabaseSeeder
fi

#exec "$@"
exec php-fpm
