<?php

use Illuminate\Http\Request;
use Http\Controllers\DemoController;
use Http\Controllers\IndustryController;
use Http\Controllers\CategoryController;
use Http\Controllers\PostController;
use Http\Controllers\SubscriptionController;
use Http\Controllers\SubscriptionExtraController;
use Http\Controllers\CommentController;
use Http\Controllers\BannerController;
use Http\Controllers\PostExtraController;
use Http\Controllers\AuthorExtraController;
use Http\Controllers\CommentExtraController;
use App\Post;
use App\Quizz;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiResource('/demo', 'DemoController');
Route::apiResource('/industries', 'IndustryController');
Route::apiResource('/categories', 'CategoryController');
Route::apiResource('/posts', 'PostController');
Route::apiResource('/subscriptions', 'SubscriptionController');
Route::apiResource('/comments', 'CommentController');
Route::apiResource('/banners', 'BannerController');

Route::get('/fresh', function () {
    $posts = Post::getActivePosts()->where('published_at', '>=', date('Y-m-d',strtotime(date("Y/m/d") . "-7 days")))->count();
    $quizz = Quizz::getActiveQuizzs()->where('published_at', '>=', date('Y-m-d',strtotime(date("Y/m/d") . "-7 days")))->count();
    return $posts+$quizz;
});
Route::post('/likes', 'PostExtraController@addlike');
Route::post('/views', 'PostExtraController@addview');
Route::post('/imageupload', 'PostExtraController@imageupload');
Route::post('/askquestion', 'AuthorExtraController@askquestion');
Route::post('/addcommentlike', 'CommentExtraController@addcommentlike');

Route::post('/exportcomment', 'SubscriptionExtraController@exportcomment');
