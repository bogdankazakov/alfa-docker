<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });




Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'MainController@index')->name('index');
Route::get('/posts/{id}', 'PostController@show')->name('main.show');
Route::get('/quizz/{id}', 'QuizzController@show')->name('quizz.show');
Route::get('/author/{id}', 'AuthorController@show')->name('author.show');
