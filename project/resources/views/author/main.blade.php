@extends('layouts.app')

@section('content')

<author
    v-bind:author="{{$author}}"
    v-bind:posts="{{$posts}}"
    v-on:pageloaded="pageLoading = false"
    v-bind:icons-avatar-url="'{{ asset('icons/avatar_icon.svg') }}'"
    v-bind:icons-heart-url="'{{ asset('icons/heart_icon.svg') }}'"
    v-bind:icons-view-url="'{{ asset('icons/view_icon.svg') }}'"
    v-bind:icons-comment-url="'{{ asset('icons/comment_icon.svg') }}'"
></author>


    @include('includes.uicomponents')
@endsection

@section('scripts')
@endsection
