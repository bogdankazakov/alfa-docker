<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8" />
  </head>
  <body>
    <h2>Вам задал вопрос — {{$name}}</h2>
    <p><b>Вопрос:</b> {{ $messages }}</p>
    <p><b>Адрес для ответа:</b> {{ $email }}</p>
  </body>
</html>
