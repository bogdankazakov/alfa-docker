@extends('layouts.app')

@section('content')
    <postpage
        v-bind:post="{{$post}}"
        v-bind:iseditable="false"
        v-on:pageloaded="pageLoading = false"
        v-on:openauth="openModal('auth')"
        v-bind:isauth="isAuth"
        v-bind:user="user"
        v-bind:backtomain="'{{ route('index', []) }}'"
        v-bind:author-url="'{{ route('author.show', ['id' => $post->author_id,]) }}'"
        v-bind:icons-back-url="'{{ asset('icons/back.svg') }}'"
        v-bind:icons-heart-url="'{{ asset('icons/heart_icon.svg') }}'"
        v-bind:icons-heart-clicked-url="'{{ asset('icons/heart_filled_icon.svg') }}'"
        v-bind:icons-view-url="'{{ asset('icons/view_icon.svg') }}'"
        v-bind:icons-comment-url="'{{ asset('icons/comment_icon.svg') }}'"
        v-bind:btn-back-category-url="'{{ route('index', ['category' => $post->category_id,]) }}'"
        v-bind:btn-back-industry-url="'{{ route('index', ['industry' => $post->industry_id,]) }}'"
        v-bind:icons-avatar-url="'{{ asset('icons/avatar_icon.svg') }}'"

        v-slot:default="slotProps"
    >
        <template>
            <div class="container">
                        <div class="row">
                            <div class="col-12 d-flex flex-column align-items-center">
                                <div class="content-container">
                                    <a v-bind:href="slotProps.btnBackCategoryUrl" class="back "  v-if="!slotProps.iseditable">
                                        <img class="mr-3" v-bind:src="slotProps.iconsBackUrl">
                                        <span class="back-txt">ВСЕ [[slotProps.postItem.category.name]]</span>
                                    </a>
                                    <h1 class="header-post" v-if="!slotProps.iseditable">[[slotProps.postItem.title]]</h1>
                                    <div class="article-tags" v-if="!slotProps.iseditable" >
                                        <a v-bind:href="slotProps.btnBackCategoryUrl" >#[[slotProps.postItem.category.name]]</a>
                                        <a v-bind:href="slotProps.btnBackIndustryUrl">#[[slotProps.postItem.industry.name]]</a>
                                    </div>
                                    <div class="article-info" v-if="!slotProps.iseditable">
                                        <div class="banner-info-item">[[slotProps.postItem.published_at]]</div>
                                        <div class="banner-info-item" >
                                              <like
                                                v-bind:item='slotProps.postItem'
                                                v-bind:type="'post'"
                                                v-bind:icons-heart-url="slotProps.iconsHeartUrl"
                                                v-bind:icons-heart-clicked-url="slotProps.iconsHeartClickedUrl"
                                                v-if="!slotProps.iseditable">
                                                </like>
                                              <div>[[slotProps.postItem.likes]]</div>
                                        </div>
                                        <div class="banner-info-item" >
                                              <img class="banner-info-icon" width="20px" v-bind:src="slotProps.iconsViewUrl">
                                              <div>[[slotProps.postItem.views]]</div>
                                        </div>
                                        <div class="banner-info-item" >
                                              <img class="banner-info-icon" width="16px" v-bind:src="slotProps.iconsCommentUrl">
                                              <div>[[slotProps.postItem.comments_count]]</div>
                                        </div>
                                    </div>

                                    @include('posts.blocks')
                                    <!-- <postblock
                                        v-for="(data, index) in slotProps.pageDatasorted"
                                        v-bind:key="index"
                                        v-bind:data="data"
                                        v-bind:pagelenght="slotProps.pageDatasorted.length"
                                        v-bind:editable="slotProps.iseditable"
                                        v-on:moveup="slotProps.blockmoveUp(data)"
                                        v-on:movedown="slotProps.blockmoveDown(data)"
                                        v-on:addblock="slotProps.blockAdd(data)"
                                        v-on:deleteblock="slotProps.blockDelete(data)"
                                        v-on:blockchange="slotProps.blockChange"
                                    ></postblock> -->

                                    <div c v-if="!slotProps.iseditable" >
                                        Автор <a :href="slotProps.authorUrl" class="article-author">[[slotProps.postItem.author_id.name]]</a>
                                    </div>

                                    <share v-bind:item='slotProps.postItem' v-if="!slotProps.iseditable"></share>
                                    <comments
                                        v-bind:id='slotProps.postItem.id'
                                        v-on:newcommentlength="slotProps.appendComment"
                                        v-on:openauth="slotProps.onOpenAuth"
                                        v-bind:isauth="slotProps.isauth"
                                        v-bind:user="slotProps.user"
                                        v-bind:icons-avatar-url="slotProps.iconsAvatarUrl"
                                        v-if="!slotProps.iseditable"
                                    ></comments>
                                </div>
                            </div>
                        </div>
                    </div>
        </template>



    </postpage>
    @include('includes.uicomponents')
@endsection

@section('scripts')
@endsection
