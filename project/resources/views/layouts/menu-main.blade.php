
        <!-- MAIN MENU -->
        <nav class="navbar navbar-expand-md navbar-light nav-padding fixed-top" v-bind:class="classNav">
            <div class="container">

                <a class="navbar-brand" href="{{ url('/') }}">
                    <img class="header-logo-risk navbar-collapse collapse" src={{ asset('img/logo_risk.png') }}>
                    <img class="header-logo navbar-collapse collapse" src={{ asset('img/logo.svg') }}>
                    <img class="navbar-toggler header-logo-mini-risk " src={{ asset('img/logomini_risk.png') }}>
                    <img class="navbar-toggler header-logo-mini " src={{ asset('img/logomini.svg') }}>
                </a>


                @if(! empty($ismain))
                    <!-- <ul class="navbar-nav d-none d-sm-block" v-on:click="showCity = !showCity"> -->
                    <ul class="navbar-nav " v-on:click="showCity = !showCity">
                        <div class="d-flex pl-3 align-items-center switcher-container" >
                            <div class="switcher-hide"></div>
                            <span >Журнал</span>
                            <label class="switch mx-3 mt-2">
                                <input type="checkbox" v-model="showCity">
                                <span class="slider round"></span>
                            </label>
                            <span class="text-secondary">Город</span>
                        </div>
                    </ul>
                @endif



                <button class="navbar-toggler" type="button" v-on:click="showMenu = true" >
                    <span class="navbar-toggler-icon"></span>
                </button>


                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->


                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#" v-on:click="openSearch">
                                    <img class="header-icon" src={{ asset('icons/search_icon.svg') }}>
                                    <span>Поиск</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <!-- <a class="nav-link" href="#">
                                    <img class="header-icon" src={{ asset('icons/user_icon.svg') }}>
                                    <span>Войти</span>
                                </a>
                            </li> -->
                            <li class="nav-item">
                                <a class="nav-link" href="#" v-on:click="openModal('subscribe')">
                                    <span>Подписка</span>
                                </a>
                            </li>
                    </ul>
                </div>
            </div>
        </nav>
