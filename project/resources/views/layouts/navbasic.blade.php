
<!-- OPEN MENU -->
<transition  name="fade" key="main-menu-open">
    <nav class="navbar navbar-expand-md navbar-light nav-padding fixed-top  nav-project bg-white" v-show="showMenu">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img class="header-logo-risk navbar-collapse collapse" src={{ asset('img/logo_risk.png') }}>
                <img class="header-logo navbar-collapse collapse" src={{ asset('img/logo.svg') }}>
                <img class="navbar-toggler header-logo-mini-risk " src={{ asset('img/logomini_risk.png') }}>
                <img class="navbar-toggler header-logo-mini " src={{ asset('img/logomini.svg') }}>
            </a>
            <div>
                <img class="menu-search-btn" src={{ asset('icons/search_icon.svg') }} v-on:click="showMenu = false; openSearch();">
                <img class="menu-close-btn" src={{ asset('icons/close_icon.svg') }} v-on:click="showMenu = false">
            </div>
        </div>
    </nav>
</transition>
