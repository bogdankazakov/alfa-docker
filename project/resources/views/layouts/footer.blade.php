
<!-- FOOTER -->
<footer>
    <div  class="container d-none d-md-block">
        <div class="row">
            <div class="col-12 d-flex justify-content-center align-items-center">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img class="footer-logo-risk " src={{ asset('img/logo_risk.png') }}>
                    <img class="footer-logo" src={{ asset('img/logo.svg') }}>
                </a>
                @foreach ($categories as $cat)
                    <a href="{{ route('index', ['category' => $cat->id,]) }}" class="footer-categories">{{ $cat->name }}</a>
                @endforeach
                <span class="flex-grow-1"></span>
                <a class="nav-link" href="#" v-on:click="openModal('subscribe')"><span class="footer-categories">Подписка</span></a>
                <a href="{{setting('site.fbgroup')}}" target="_blank" class="footer-secondary"><img class="footer-fb" src={{ asset('icons/facebook.svg') }}></a>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-12 d-flex justify-content-between align-items-center">
                <div>
                    <img class="mr-2" src={{ asset('icons/deluxe.svg') }}>
                    <a href="https://deluxe-interactive.ru" target="_blank" class="footer-secondary">Сделано в Deluxe Interactive</span>
                </div>
                <span class="footer-secondary">© Институт Риска. Проект компании АльфаСтрахование.</span>
            </div>
        </div>
    </div>

    <div  class="container d-block d-md-none">
        <div class="row">
            <div class="col-4 d-flex flex-column justify-content-start align-items-start pt-1">
                <a class="" href="{{ url('/') }}"><img class="footer-logo-risk mb-3" src={{ asset('img/logo_risk.png') }}></a>
                <a class="" href="{{ url('/') }}"><img class="footer-logo" src={{ asset('img/logo.svg') }}></a>
            </div>



            <div class="col-6 d-flex justify-content-center align-items-center">
                <div class="row row-cols-2">
                    @foreach ($categories as $cat)
                    <div class="col">
                        <a href="{{ route('index', ['category' => $cat->id,]) }}" class=" footer-categories">{{ $cat->name }}</a>
                    </div>
                    @endforeach
                    <div class="col">
                        <a class="nav-link col  footer-categories" href="#" v-on:click="openModal('subscribe')">Подписка</a>
                    </div>
                </div>


            </div>
        </div>

        <div class="row mt-3">
            <div class="col-12 d-flex justify-content-between align-items-center">
                <span class="footer-secondary">© Институт Риска. <br />Проект компании АльфаСтрахование.</span>
                <a href="{{setting('site.fbgroup')}}" target="_blank" class="footer-secondary"><img class="footer-fb" src={{ asset('icons/facebook.svg') }}></a>

            </div>
        </div>
        <div class="row mt-2">
            <div class="col-12 d-flex justify-content-between align-items-center">
                <div>
                    <img class="mr-2" src={{ asset('icons/deluxe.svg') }}>
                    <a href="https://deluxe-interactive.ru" target="_blank" class="footer-secondary">Сделано в Deluxe Interactive</span>
                </div>
            </div>
        </div>
    </div>

</footer>
