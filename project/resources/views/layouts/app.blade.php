@extends('layouts.base')

@section('body')
<body >
    @include('preloader.top')
    <!--Preloader-->
        <div class="loaderArea" id="preloader" >
        <!-- <div class="loaderArea" v-if="pageLoading"> -->
            <div class="loader">
                <div id="lottie"></div>
            </div>
        </div>
    <!--End-->
    <div id="app">
        <div
            v-bind:class="classBody"
            tabindex="-1"
            v-on:keydown.shift.56.capture.prevent.stop="activateLogout"
        >

            @include('layouts.menu-main')
            @include('layouts.navbasic')

            @yield('content')

            @include('layouts.footer')

        </div>

    </div>
    @include('preloader.bottom')
    @yield('scripts')
</body>
@endsection
