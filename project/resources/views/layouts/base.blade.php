<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-167356289-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-167356289-1');
    </script>
    <!-- END -->

    <title>{{ isset($title) ? $title : setting('site.title') }}</title>

    <!-- Meta -->
    <meta name="keywords" content="{{ isset($keywords) ? $keywords : setting('site.keywords') }}" />
    <meta name="description" content="{{ isset($description) ? $description : setting('site.description') }}" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <!-- OpenGraph -->
    <link rel="canonical" href="<?php echo URL::current(); ?>" />
    <meta property="og:locale" content="ru_RU" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{ isset($title) ? $title : setting('site.title') }}" />
    <meta property="og:description" content="{{ isset($description) ? $description : setting('site.description') }}" />
    <meta property="og:url" content="<?php echo URL::current(); ?>" />
    <meta property="og:site_name" content="{{setting('site.title')}}" />
    <meta property="og:image" content="{{ Request::root() }}/storage/{{ isset($image) ? $image : setting('site.share_img') }}" />
    <meta name="twitter:card" content="{{ isset($title) ? $title : setting('site.title') }}" />
    <meta name="twitter:description" content="{{ isset($description) ? $description : setting('site.description') }}" />
    <meta name="twitter:title" content="{{ isset($title) ? $title : setting('site.title') }}"/>
    <meta name="twitter:site" content="{{setting('site.title')}}" />
    <meta name="twitter:image" content="{{ Request::root() }}/storage/{{ isset($image) ? $image : setting('site.share_img') }}" />

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
       (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
       m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
       (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

       ym(66724210, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
       });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/66724210" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    
    <!-- Scripts -->
    @include('layouts.scripts')
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('font/proximanova/stylesheet.css') }}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/post.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet"> -->

    <link rel="icon" href="{{ asset('favicon.ico') }}">
</head>

@yield('body')
</html>
