@extends('layouts.app')

@section('content')
    <mainpage
        v-on:pageloaded="pageLoading = false"
        v-on:open-filter="openFilter"
        v-on:close-filter="closeFilter"
        v-bind:icons-filter-url="'{{ asset('icons/filter_icon.svg') }}'"
        v-bind:icons-heart-url="'{{ asset('icons/heart_icon.svg') }}'"
        v-bind:icons-view-url="'{{ asset('icons/view_icon.svg') }}'"
        v-bind:icons-comment-url="'{{ asset('icons/comment_icon.svg') }}'"
        v-bind:icons-heart-white-url="'{{ asset('icons/heart_white_icon.svg') }}'"
        v-bind:icons-view-white-url="'{{ asset('icons/view_white_icon.svg') }}'"
        v-bind:icons-comment-white-url="'{{ asset('icons/comment_white_icon.svg') }}'"
        v-bind:icons-close-url="'{{ asset('icons/close_icon.svg') }}'"
        v-bind:icons-link-url="'{{ asset('icons/banner_link_icon.svg') }}'"
        v-bind:icons-articles-url="'{{ asset('icons/articles_icon.svg') }}'"
        v-bind:icons-star-url="'{{ asset('icons/star_icon.svg') }}'"
        v-bind:icons-new-url="'{{ asset('icons/new_icon.svg') }}'"
        v-bind:icons-news-url="'{{ asset('icons/news_icon.svg') }}'"
        v-bind:icons-lamp-url="'{{ asset('icons/lamp_icon.svg') }}'"
        v-bind:city-url="'{{ asset('img/city.jpg') }}'"
        v-bind:city-dot-url="'{{ asset('icons/city_dot.svg') }}'"
        v-bind:show-city.sync="showCity"
    ></mainpage>
    @include('includes.uicomponents')
@endsection

@section('scripts')
@endsection
