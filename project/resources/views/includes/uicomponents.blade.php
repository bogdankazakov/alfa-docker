<!-- MENU OPEN -->
<transition  name="fade">
    <section class="menu" v-show="showMenu">
        <div class="menu-bg"></div>
        <div class=" menu-body">
            <div class="menu-top">
                <div class="container">
                    <!-- <div class="row">
                        <a href="{{ route('index', []) }}" class="col-12 menu-mobile-nav-big" >
                            Журнал
                        </a>
                    </div>
                    <div class="row">
                        <a href="{{ route('index', ['city' => 'show']) }}" class="col-12 menu-mobile-nav-big" >
                            Город
                        </a>
                    </div> -->
                    <div class="row row-cols-2 menu-mobile-nav-small">
                        <div class="col ">
                            <a href="{{ route('index', []) }}"><p>Свежее</p></a>
                        </div>
                        @foreach ($categories as $cat)
                            <div class="col">
                                <a href="{{ route('index', ['category' => $cat->id,]) }}"><p> {{$cat->name}}</p></a>
                            </div>
                        @endforeach


                    </div>
                </div>
            </div>
            <div class="menu-bottom">
                <div class="container ">
                    <div class="row">
                        <div class="col-12" >
                                <subscription></subscription>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>
</transition>

<transition name="fade">
    <search
        v-bind:show="showSearch"
        v-on:searchclose="closeSearch"
        v-show="showSearch"
        v-bind:btn-back-url="'{{ route('index') }}'"
        v-bind:logo-url="'{{ asset('img/logo.svg') }}'"
        v-bind:logo-mini-url="'{{ asset('img/logomini.svg') }}'"
        v-bind:icons-close-url="'{{ asset('icons/close_icon.svg') }}'"
        v-bind:icons-heart-url="'{{ asset('icons/heart_icon.svg') }}'"
        v-bind:icons-view-url="'{{ asset('icons/view_icon.svg') }}'"
        v-bind:icons-comment-url="'{{ asset('icons/comment_icon.svg') }}'"
    ></search>
</transition>





<!-- MODAL AUTH -->
<transition  name="fade" >
    <auth
        v-on:authenticated="onAuthentication"
        v-on:logout="isAuth = false"
        v-bind:show-auth="showAuth"
        v-bind:show-logout.sync="showLogout"
        v-bind:close-icon-url="'{{ asset('icons/close_icon.svg') }}'"
        v-on:close-modal="closeModal('auth')"

    ></auth>
</transition>


<!-- MODAL SUBSCRIBE -->
<transition  name="fade" >
    <section v-if="showSubscribe" class="modal-container">
        <div class="modal-container-bg"  v-on:click="closeModal('subscribe')"></div>
        <div class="project-modal">
            <img class="project-modal-close-btn" src={{ asset('icons/close_icon.svg') }} v-on:click="closeModal('subscribe')">
            <subscription></subscription>
        </div>
    </section>
</transition>
