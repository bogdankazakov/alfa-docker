<div class="filter-container filter-sticky">
    <div class="filter-header">РУБРИКИ</div>
    <div class="filter-items">
        <div class="filter-item "
            v-for="item in categories"
            :key="item.parent.id"
        >

            <div
                class="filter-item-main"
                v-bind:class="{ active: item.parent.isActive, selected: item.parent.isSelected, }"
                v-on:click="setActive(item.parent.id);"
            >

                <img
                    class="filter-item-icon" src={{ asset('icons/new_icon.svg') }}
                    v-if="item.parent.id == 0"
                >
                <img
                    class="filter-item-icon" src={{ asset('icons/star_icon.svg') }}
                    v-else-if="item.parent.id == 1"
                >
                <img
                    class="filter-item-icon" src={{ asset('icons/articles_icon.svg') }}
                    v-else
                >




                <span class="filter-item-text">[[item.parent.name]] </span>

                <div class="project-dropdown"
                    v-bind:class="{ dropdownrotate: !item.isDropdownexpanded }"
                    v-show="hasChildren(item)"
                ></div>
                <span
                    class="badge badge-pill badge-danger project-badge-pill"
                    v-show="item.parent.id == 0"
                >[[freshCounter]]</span>

            </div>
            <transition name='fade'>
                <div class="filter-sublist" v-show="item.isDropdownexpanded & hasChildren(item) ">
                      <div
                            class="filter-sublist-item"
                            v-for="el in item.children"
                            :key="el.id"
                            v-bind:class="{ active: el.isActive }"
                            v-on:click="setActive(el.id)"
                      >
                          <div class="filter-sublist-item-icon"></div>
                          <span class="filter-sublist-item-text">[[el.name]]</span>
                      </div>
                </div>
            </transition>

        </div>

    </div>
    <div class="filter-header">ОТРАСЛИ</div>
    <div class="w-100">
        <div
            class="filter-item"
            v-for="item in industries"
            :key="item.id"
        >
            <label class="checkbox-container">[[item.name]]
                <input type="checkbox" :id="item.id" :value="item.id" v-model="industryActive">
                <span class="checkmark"></span>
            </label>
        </div>

    </div>
</div>
