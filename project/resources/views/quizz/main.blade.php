@extends('layouts.app')

@section('content')
    <quizz
        v-bind:quizz="{{$quizz}}"
        v-on:pageloaded="pageLoading = false"
        v-bind:icons-reload-url="'{{ asset('icons/reload_icon.svg') }}'"
        v-bind:icons-back-url="'{{ asset('icons/back.svg') }}'"
        v-bind:icons-heart-url="'{{ asset('icons/heart_icon.svg') }}'"
        v-bind:icons-heart-clicked-url="'{{ asset('icons/heart_filled_icon.svg') }}'"
        v-bind:icons-view-url="'{{ asset('icons/view_icon.svg') }}'"
        v-bind:mainimage-url="'{{ asset('storage/'.$quizz->image) }}'"
        v-bind:btn-back-category-url="'{{ route('index', ['category' => '1',]) }}'"
        v-bind:btn-back-industry-url="'{{ route('index', ['industry' => $quizz->industry_id,]) }}'"
    ></quizz>
    @include('includes.uicomponents')
@endsection

@section('scripts')
@endsection
