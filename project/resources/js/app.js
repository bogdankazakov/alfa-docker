/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('mainpage', require('./components/MainPage.vue').default);
Vue.component('search', require('./components/Search.vue').default);
Vue.component('postpage', require('./components/PostPage.vue').default);
Vue.component('quizz', require('./components/QuizzPage.vue').default);
Vue.component('author', require('./components/Author.vue').default);

Vue.component('left-filter', require('./components/includes/Filter.vue').default);
Vue.component('banners', require('./components/includes/Banners.vue').default);
Vue.component('dropdown', require('./components/includes/DropDown.vue').default);
Vue.component('postblock', require('./components/includes/PostPageBlock.vue').default);
Vue.component('quill', require('./components/includes/Quill.vue').default);
Vue.component('share', require('./components/includes/Share.vue').default);
Vue.component('like', require('./components/includes/Like.vue').default);
Vue.component('comments', require('./components/includes/Comments.vue').default);
Vue.component('subscription', require('./components/includes/Subscription.vue').default);
Vue.component('auth', require('./components/includes/Auth.vue').default);
Vue.component('dot', require('./components/includes/Dot.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
   el: '#app',
   delimiters: ["[[","]]"],
   data: {
       pageLoading: true,
       classBody:{},
       classNav:{},
       showCity:false,

       isAuth: false,
       user: {},
       showLogout: false,

       showSearch: false,
       showAuth: false,
       showMenu: false,
       showSubscribe: false,
       showFilter: false,

       postBodyData:null,
       postPageDefaultData:[
           {
               id: 0,
               order: 0,
               typeId: 1,
               content: {
                   headerimage: 'demo/articles.png',
               },
           },
           {
               id: 1,
               order: 1,
               typeId: 0,
               content: {
                   text: '<p><strong>Lorem ipsum dolor</strong></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
               },
           },
           {
               id: 2,
               order: 2,
               typeId: 3,
               content: {
                   text: 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?',
               },
           },
           {
               id: 3,
               order: 3,
               typeId: 3,
               content: {
                   text: 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?',
               },
           },
           {
               id: 4,
               order: 4,
               typeId: 4,
               content: {
                   text: 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?',
               },
           },
           {
               id: 5,
               order: 5,
               typeId: 4,
               content: {
                   text: 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?',
               },
           },
           {
               id: 6,
               order: 6,
               typeId: 5,
               content: {
                   text: '<iframe width="560" height="315" src="https://www.youtube.com/embed/6ardHZ-vO7s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
               },
           },
       ],

   },
   created:function(){
       this.setColor();
       axios.defaults.xsrfCookieName = 'csrftoken';
       axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";

   },
   beforeMount: function() {
       try {
           var data = this.$el.querySelector('[name="body"]').value
           if(data.length >0) {
                this.postBodyData = data
           } else{
               this.postBodyData = JSON.stringify(this.postPageDefaultData)
           }
       }
       catch (e) {}
   },
   mounted: function(){
        // window.scrollTo(0, 0);
   },
   computed:{
       postBodyDataFormated:function(){
           var data ={
               body: this.postBodyData,
               title: 'Test title',
               views: 1000,
               likes: 1000,
               industry:{
                   name: 'Test industry'
               },
               author_id:{
                   name: 'Test author'
               }
           }
           return data
       },
   },
   methods:{
       activateLogout:function(){
           this.showAuth = true
           this.showLogout = true
       },
       onAuthentication:function(e){
           this.isAuth = true
           this.user = e
       },
       setColor: function(){
           var url = (window.location.href).split("/")

           if (url.includes("quizz")){
               this.classBody = {
                  body: false,
                  "body-white": true
               }
               this.classNav = {
                  "nav-project-gray": false,
                  "nav-project-white": true
               }
           } else if (url.includes("posts")){

               this.classBody = {
                  body: false,
                  "body-white": true
               }
               this.classNav = {
                  "nav-project-gray": false,
                  "nav-project-white": true
               }
           } else{

               this.classBody = {
                  body: true,
                  "body-white": false
               }
               this.classNav = {
                  "nav-project-gray": true,
                  "nav-project-white": false
               }
           }
       },
       openSearch:function(){
           this.showSearch = true
           this.classBody.modalopen  = true
       },
       closeSearch:function(){
           this.showSearch = false
           this.classBody.modalopen  = false
       },
       openFilter:function(){
           this.classBody  = {
               body:true,
               modalopen:true,
           }
       },
       closeFilter:function(){
           this.classBody  = {
               body:true,
               modalopen:false,
           }
       },
       openModal:function(id, event){
           if (id === "subscribe"){
               this.showSubscribe = true
               this.classBody.modalopen  = true
           }
           if (id === "postedit"){
               this.showPostEdit = true
               this.classBody.modalopen  = true
           }
           if (id === "auth"){
               this.showAuth = true
               this.classBody.modalopen  = true
           }

       },
       closeModal:function(id){
           if (id === "subscribe"){
               this.showSubscribe = false
               this.classBody.modalopen  = false
           }
           if (id === "postedit"){
               this.showPostEdit = false
               this.classBody.modalopen  = false
           }
           if (id === "auth"){
               this.showAuth = false
               this.classBody.modalopen  = false
           }
       },
       postPagechange:function(data){
           this.postBodyData = data
       },
   },
   watch:{
       pageLoading(newValue){
           if (!newValue){
               // document.getElementById('preloader').remove()
               $('#preloader').remove();
           }
       }
   },
  })
