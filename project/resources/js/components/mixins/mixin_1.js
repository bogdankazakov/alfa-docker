export default {
    methods: {
        isElementVisible:function (el) {
            var rect     = el.getBoundingClientRect(),
                vWidth   = window.innerWidth || document.documentElement.clientWidth,
                vHeight  = window.innerHeight || document.documentElement.clientHeight,
                efp      = function (x, y) { return document.elementFromPoint(x, y) };

            // Return false if it's not in the viewport
            if (rect.right < 0 || rect.bottom < 0
                    || rect.left > vWidth || rect.top > vHeight)
                return false;

            // Return true if any of its four corners are visible
            return (
                  el.contains(efp(rect.left,  rect.top))
              ||  el.contains(efp(rect.right, rect.top))
              ||  el.contains(efp(rect.right, rect.bottom))
              ||  el.contains(efp(rect.left,  rect.bottom))
            );
        },
        postFormater: function(list){
            for (const item of list){
                item.published_at = moment(item.published_at  + ' Z').locale('ru').format("D MMM");

                item.tags = []
                var category = null

                try {
                   var img_arr = item.image.split(".");
                   img_arr.splice(1, 0, '-small');
                   img_arr[img_arr.length - 1] = "."+img_arr[img_arr.length - 1]
                   item.image = '/storage/' + img_arr.join("");
                   item.imagecss = "background-image: url(" + item.image +")"
                }
                catch (e) {}

                try {
                   var img_arr = item.features_icon.split(".");
                   img_arr.splice(1, 0, '');
                   img_arr[img_arr.length - 1] = "."+img_arr[img_arr.length - 1]
                   item.features_icon = '/storage/' + img_arr.join("");
                   item.features_iconcss = "background-image: url(" + item.features_icon +")"
                }
                catch (e) {}


                if(item.industry !== null ){
                    item.tags.push(
                        {
                            'id': item.industry.id,
                            'type': 1,
                            'name': item.industry.name
                        }
                    )
                }

                if(item.category !== null){
                    item.tags.push(
                        {
                            'id': item.category.id,
                            'type': 0,
                            'name': item.category.name
                        }
                    )
                }
            }
            return list
        },
        setTagLink(tag){
            if (tag.type === 0){
                return "/?category=" + tag.id
            }
            if (tag.type === 1){
                return "/?industry=" + tag.id
            }
        },
        setLink:function(post){
            if (post.hasOwnProperty("author_id")){
                return '/posts/' + post.id
            }  else{
                return '/quizz/' + post.id
            }
        },
    }
};
