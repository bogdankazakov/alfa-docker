$(function() {

$('#carouselExampleIndicators').carousel('pause')


    // ------ BANNERS COMPONENT
    Vue.component('banners', {
         delimiters: ["[[","]]"],
         template: '#banners-template',
         props: [],
         data: function () {
             return {
                 banners:[],
                 url:"/api/banners",
             }
         },
         mounted:function(){
             axios({
                 method: 'get',
                 url: this.url,
             })
             .then(response => {
                 this.banners = this.bannerFormater(response.data.banners)
                 this.$emit('banner-loaded');
             })
             .catch(response => {
                 console.log('error!');
             })
         },
         methods:{
             bannerFormater: function(list){
                 var formatedList=[]
                 for (const item of list){
                     if (item.type === 'POST'){
                         formatedList.push(
                             {
                                 title:item.related_item.title,
                                 category:item.related_item.category.name,
                                 industry:item.related_item.industry.name,
                                 published_at:moment(item.related_item.published_at + ' Z').locale('ru').format("D MMM"),
                                 likes:item.related_item.likes,
                                 views:item.related_item.views,
                                 comments:item.related_item.comments_count,
                                 image: '/storage/'.concat(item.image) ,
                                 link: item.link,
                             }
                         )
                     } else if(item.type === 'QUIZZ'){
                         formatedList.push(
                             {
                                 title:item.related_item.title,
                                 category:'Интерактив',
                                 industry:item.related_item.industry.name,
                                 published_at:moment(item.related_item.published_at + ' Z').locale('ru').format("D MMM"),
                                 likes:item.related_item.likes,
                                 views:item.related_item.views,
                                 image: '/storage/'.concat(item.image) ,
                                 link: item.link,
                             }
                         )
                     }
                      else{
                         formatedList.push(
                             {
                                 title:item.title,
                                 description:item.description,
                                 image: '/storage/'.concat(item.image) ,
                                 link: item.link,
                             }
                         )
                      }
                 }
                 return formatedList
             },
         },
     })

    // ------ LIKE COMPONENT
    Vue.component('like', {
         delimiters: ["[[","]]"],
         template: '#like-template',
         props: ['item', 'type'],
         data: function () {
             return {
                 likeDisabled:false,
                 url:"/api/likes",
             }
         },
         methods:{
             addLike:function(){
                 axios({
                     method: 'post',
                     url: this.url,
                     data: {
                         id: this.item.id,
                         type: this.type
                     },
                 })
                 .then(response => {
                     this.item.likes = response.data.total
                     this.likeDisabled = true
                 })
                 .catch(response => {
                     console.log('error!');
                 })
             },
         },
     })

    // ------ SHARE COMPONENT
    Vue.component('share', {
         delimiters: ["[[","]]"],
         template: '#share-template',
         props: ['item'],
         data: function () {
             return {
             }
         },
         methods:{
             share: function(tag){
                 if(tag === "FB"){
                     var base = 'https://www.facebook.com/sharer/sharer.php?u='
                     var url  = window.location.href
                     var fullurl = base.concat(url)
                     window.open(fullurl, '_blank');
                 }
                 if(tag === "VK"){
                     var base = 'https://vk.com/share.php?url='
                     var url  = window.location.href
                     var fullurl = base.concat(url)
                     window.open(fullurl, '_blank');
                 }
                 if(tag === "TG"){
                     var base = 'tg://msg_url?'
                     var url  = window.location.href
                     var msg  = this.item.title
                     var fullurl = base.concat('url=',url,'&text=',msg)
                     window.open(fullurl, '_blank');
                 }
                 if(tag === "TW"){
                     var base = 'http://twitter.com/share?text='
                     var snipet = this.item.title
                     var url  = window.location.href
                     var fullurl = base.concat(snipet,'&url=',url)
                     window.open(fullurl, '_blank');
                 }
             },
         },
     })

    // ------ QUIZZ COMPONENT
    Vue.component('quizz', {
         delimiters: ["[[","]]"],
         template: '#quizz-page',
         props: ['quizz',],
         data: function () {
             return {
                 quizzItem: null,
                 index: 0,
                 currentAnswer: null,
                 result:[],
                 isDisabled: true,
                 showResults: false,
                 url:{
                     views: "/api/views",
                 },
             }
         },
         created: function(){
             this.quizzItem = this.quizz
             this.quizzItem.published_at = moment(this.quizz.published_at  + ' Z').locale('ru').format("D MMM");
         },
         mounted: function(){
                 axios({
                     method: 'post',
                     url: this.url.views,
                     data: {
                         id: this.quizzItem.id,
                         type: 'quizz',
                     },
                 })
                 .then(response => {
                     this.quizzItem.views = response.data.total
                     this.$emit('pageloaded');
                 })
                 .catch(response => {
                     console.log('error!');
                 })

         },
         computed: {
             currentQuestion:function(){
                 return this.quizzItem.questions[this.index]
             },
         },
         methods:{
             checkAnswer:function(answer){
                 for (const item of this.result){
                     if (item.questionId === this.currentQuestion.id)  {
                         return null
                     }
                 }

                 this.result.push({
                     questionId: this.currentQuestion.id,
                     answerId: answer.id,
                     result: answer.isAnswer,
                 })

                 this.currentAnswer = {
                     id: answer.id,
                     result: answer.isAnswer
                 }

                 this.isDisabled = false
             },
             classAnswer:function(answer){
                 if (this.currentAnswer !== null){
                     if( answer.id === this.currentAnswer.id){
                         return {
                             correct: this.currentAnswer.result,
                             error: !this.currentAnswer.result
                         }
                     }
                }
             },
             nextQuestion:function(){
                 if ((this.index + 1) === this.quizzItem.questions.length){
                     this.showResults = true
                 } else{
                     this.currentAnswer = null
                     this.isDisabled = true
                     this.index +=1
                 }
             },
             countResult:function(){
                 list = []
                 for (const item of this.result){
                     if (item.result === true)  {
                         list.push(1)
                     }
                 }
                 return list.length
             },
         },
     })

    // ------ QUILL COMPONENT
    Vue.component('quill', {
          delimiters: ["[[","]]"],
          template: '#quill-template',
          props: [],
          data: function () {
              return {
              }
          },

          mounted: function(){


              try {
                  let quill = new Quill(this.$el, {
                      theme: 'snow',
                  });
                  quill.on('text-change', (delta, oldDelta, source) => {
                      let html = quill.root.innerHTML;
                      this.$emit('quillchange', html, delta, oldDelta, source);
                  });
              }
              catch (e) {}




          },
      })

      // ------ POSTBLOCK COMPONENT
    Vue.component('postblock', {
           delimiters: ["[[","]]"],
           template: '#post-block',
           props: ['data', 'pagelenght', 'editable'],
           data: function () {
               return {
                   isVisible: false,
                   isEditmode: false,
                   types:[
                       {
                           id:0,
                           name: 'Текст - Основной'
                       },
                       {
                           id:1,
                           name: 'Картинка -  Заглавная'
                       },
                       {
                           id:2,
                           name: 'Картинка -  В текст'
                       },
                       {
                           id:3,
                           name: 'Текст - Выделение курсивом'
                       },
                       {
                           id:4,
                           name: 'Текст - Выделение красной полосой'
                       },
                       {
                           id:5,
                           name: 'Видео'
                       },
                   ],
                   url:{
                       headerImage: "/api/imageupload",
                   },
               }
           },
           methods:{
               quilltextChanged:function(html, delta, oldDelta, source) {
                   this.data.content.text = html
                   // this.$emit('datachange', html);
               },
               imageUpload: function(e){
                    var formData = new FormData();
                    var imagefile = e.target.files[0];

                    formData.append("image", imagefile);

                    axios({
                        method: 'post',
                        url: this.url.headerImage,
                        data: formData,
                        headers: {
                          'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then(response => {
                        this.data.content.headerimage = response.data[0]
                        this.$emit('blockchange', this.data);
                    })
                    .catch(response => {
                        console.log('error!');
                    })

               },
           },
       })

    // ------ POSTPAGE COMPONENT
    Vue.component('postpage', {
           delimiters: ["[[","]]"],
           template: '#post-page',
           props: ['post', 'iseditable', 'isauth'],
           data: function () {
               return {
                   url:{
                       views: "/api/views",
                   },
                   postItem:'',
                   postPageData:''
               }
           },
           beforeMount: function() {
               this.postItem = this.post
               this.postPageData = JSON.parse(this.post.body)
           },
           mounted: function(){
               this.postItem.published_at = moment(this.postItem.published_at  + ' Z').locale('ru').format("D MMM");

               if(!this.iseditable){
                   axios({
                       method: 'post',
                       url: this.url.views,
                       data: {
                           id: this.post.id,
                           type: 'post',

                       },
                   })
                   .then(response => {
                       this.postItem.views = response.data.total
                       this.$emit('pageloaded');
                   })
                   .catch(response => {
                       console.log('error!');
                   })
               }



           },
           computed: {
               pageDatasorted:  function(){
                   var list = this.postPageData.slice()
                   list.sort(function (a, b) {
                     if (a.order > b.order) {
                       return 1;
                     }
                     if (a.order < b.order) {
                       return -1;
                     }
                     return 0;
                   });
                   return list
               },
           },
           methods:{
               blockmoveUp: function(instance){
                   for (const item of this.postPageData){
                       if(item.order === instance.order){
                            var currentElem = item
                       }
                       if(item.order === (instance.order-1)){
                            var upperElem = item
                       }
                   }
                   for (const item of this.postPageData){
                       if(item === currentElem){
                            item.order -=1
                       }
                       if(item === upperElem){
                            item.order +=1
                       }
                   }
               },
               blockmoveDown:function(instance){
                   for (const item of this.postPageData){
                       if(item.order === instance.order){
                            var currentElem = item
                       }
                       if(item.order === (instance.order+1)){
                            var upperElem = item
                       }
                   }
                   for (const item of this.postPageData){
                       if(item === currentElem){
                            item.order +=1
                       }
                       if(item === upperElem){
                            item.order -=1
                       }
                   }
               },
               blockAdd:function(instance){
                   function getMaxOfArray(numArray) {
                     return Math.max.apply(null, numArray);
                   }
                   var idList = []
                   for (const item of this.postPageData){
                       idList.push(item.id)
                   }

                   var newItemOrder = instance.order + 1
                   var maxOrder = this.postPageData.length
                   i=1
                   while ((maxOrder-i) >= newItemOrder){
                       for (const item of this.postPageData){
                           if((maxOrder-i) === item.order){
                               item.order +=1
                           }
                       }
                       i +=1
                   }
                   this.postPageData.push(
                       {
                           id: (getMaxOfArray(idList) + 1),
                           order: (instance.order + 1),
                           typeId: 0,
                           content: {
                               text:"Новый Пустой блок"
                           }
                       },
                   )
               },
               blockDelete:function(instance){
                   var deletedItemOrder = instance.order
                   var maxOrder = this.postPageData.length
                   i=1
                   while ((maxOrder) > (deletedItemOrder+i)){
                       for (const item of this.postPageData){
                           if((deletedItemOrder+i) === item.order){
                               item.order -=1
                           }
                       }
                       i +=1
                   }

                   i=0
                   for (const item of this.postPageData){
                       if(instance.id === item.id){
                           this.postPageData.splice(i, 1);
                       }
                       i+=1
                   }
               },
               blockChange:function(e){
                   i=0
                   for (const item of this.postPageData){
                       if(e.id === item.id){
                           this.postPageData.splice(i, 1, e);
                       }
                       i+=1
                   }

               },
               appendComment:function(e){
                   this.postItem.comments_count  =  this.postItem.comments_count  +  1
               },
           },
           watch:{
               postPageData:{
                  deep: true,
                  handler: function (newValue, oldValue) {
                      this.$emit('postpagechange', JSON.stringify(newValue));
                  },
              },
          },

       })

    // ------ DROPDOWN COMPONENT
    Vue.component('dropdown', {
         delimiters: ["[[","]]"],
         template: '#dropdown-template',
         props: ['data'],
         data: function () {
             return {
                 isVisible: false,
             }
         },
         computed: {
             value:function(){
                 for (const item of this.data){
                     if (item.isSelected)  {
                         return item.name
                     }
                 }
             },
         },
         methods:{
             setActive:function(id){
                 for (const item of this.data){
                     if (item.id === id)  {
                         item.isSelected = true
                         this.isVisible = false
                     } else{
                         item.isSelected = false
                     }
                 }

             },
         },
     })

    // ------ SUBSCRIPTION COMPONENT
    Vue.component('subscription', {
      delimiters: ["[[","]]"],
      template: '#subscription-template',
      props: ['quizz',],
      data: function () {
          return {
              email: '',
              isEmailError: '',
              url: "/api/subscriptions",
              thankYou:false,
              btnActive: false,
              loaderisVisible: false,
          }
      },
      mounted: function(){
      },
      computed: {

      },
      methods:{
          validateEmail:function(mail){
              if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)){
                  return (true)
              }
              return (false)
          },
          submitSubscription:function(){
              this.btnActive = false
              this.loaderisVisible =true
                  axios({
                      method: 'post',
                      url: this.url,
                      data: {
                          email:this.email,
                      },
                  })
                  .then(response => {
                      this.thankYou = true
                  })
                  .catch(response => {
                      console.log('error!');
                  })
          },
      },
      watch:{
          email:_.debounce(function(newValue, oldValue){
              if(!this.validateEmail(newValue)){
                  this.isEmailError = "Вы ввели неверный email!"
                  this.btnActive = false
              } else{
                  this.isEmailError = ''
                  this.btnActive = true
              }
          }, 500),
      },
    })


        // ------ AUTH COMPONENT
        Vue.component('auth', {
             delimiters: ["[[","]]"],
             template: '#auth-template',
             props: [''],
             data: function () {
                 return {
                     success: false,
                 }
             },
             computed: {

             },
             methods:{
                 test:function(){
                     this.success = true
                     this.$emit('authenticated')
                 },
             },
         })


    // ------ COMMENT COMPONENT
    Vue.component('comments', {
      delimiters: ["[[","]]"],
      template: '#comments-template',
      props: ['id', 'isauth'],
      data: function () {
          return {
              showInput:false,
              isInputFocused: false,
              isDisabled: false,
              isBtnDisabled: false,
              isBtnFocused: false,
              comment:'',
              url: "/api/comments",
              allComments: null,
          }
      },
      mounted: function(){
          this.getAllComments();
      },
      computed: {
          allCommentsLength:function(){
            if (this.allComments){
                return this.allComments.length
            } else{
                return 0
            }
          },
      },
      methods:{
          openInput:function(){
              this.showInput = true
              this.$nextTick(() => this.$refs.commentInput.focus())
          },
          autoexpand:function (event) {
                var field = event.target
              // Reset field height
              field.style.height = 'inherit';

              // Get the computed styles for the element
              var computed = window.getComputedStyle(field);

              // Calculate the height
              var height = parseInt(computed.getPropertyValue('border-top-width'), 10)
                           + parseInt(computed.getPropertyValue('padding-top'), 10)
                           + field.scrollHeight + 20
                           + parseInt(computed.getPropertyValue('padding-bottom'), 10)
                           + parseInt(computed.getPropertyValue('border-bottom-width'), 10);

              field.style.height = height + 'px';

          },
          sendComment:function(){
              if (this.isauth){
                  this.sender()
              } else {
                  this.$emit('openauth')
              }
          },
          sender: function(){
              this.isDisabled = true
              this.isBtnDisabled = true
              axios({
                  method: 'post',
                  url: this.url,
                  data: {
                    comment:this.comment,
                    post_id:this.id
                  },
              })
              .then(response => {

                  this.getAllComments();
                  this.$emit('newcommentlength');

              })
              .catch(response => {
                  console.log('error!');
              })
          },
          getAllComments:function(){
                axios({
                    method: 'get',
                    url: this.url,
                    params: {
                      post_id:this.id
                    },
                })
                .then(response => {
                    this.allComments = this.commentFormater(response.data.comments);
                    this.isDisabled = false
                    this.isBtnDisabled = false
                    this.showInput = false
                    this.isBtnFocused = false
                    this.$nextTick(() => this.$refs.commentInput.style.height = '140px')
                    this.comment = ''
                })
                .catch(response => {
                    console.log('error!');
                })
          },
          commentFormater: function(list){
              for (const item of list){
                  item.created_at = moment(item.created_at + ' Z').locale('ru').fromNow();
              }
              return list
          },
      },
      watch:{
          isauth:function(newValue, oldValue){
              if(newValue){
                  this.sendComment()
              }
          },
          isInputFocused: _.debounce(function(){
                  if(!this.isInputFocused){
                      if(this.isDisabled){
                          this.isBtnFocused =  true
                      }else{
                          this.isBtnFocused = false
                      }
                  }else{
                      this.isBtnFocused =  true
                  }
          }, 150),
      },
    })

    var baseMixin = {
      methods: {
          postFormater: function(list){
              for (const item of list){
                  item.published_at = moment(item.published_at  + ' Z').locale('ru').format("D MMM");

                  item.tags = []
                  var category = null

                  try {
                     var img_arr = item.image.split(".");
                     img_arr.splice(1, 0, '-small');
                     img_arr[img_arr.length - 1] = "."+img_arr[img_arr.length - 1]
                     item.image = '/storage/' + img_arr.join("");
                  }
                  catch (e) {}

                  if(item.industry !== null ){
                      item.tags.push(
                          {
                              'id': 0,
                              'name': item.industry.name
                          }
                      )
                  }

                  if(item.category !== null){
                      item.tags.push(
                          {
                              'id': 0,
                              'name': item.category.name
                          }
                      )
                  }
              }
              return list
          },
          setLink:function(post){
              if (post.hasOwnProperty("author_id")){
                  return '/posts/' + post.id
              }  else{
                  return '/quizz/' + post.id
              }
          },
      }
    }

     // ------ MAINPAGE COMPONENT
    Vue.component('mainpage', {
          extends: baseMixin,
          delimiters: ["[[","]]"],
          template: '#mainpage-template',
          props: [],
          data: function () {
              return {
                  baseLoading: true,
                  bannerLoading: true,
                  postLoading: true,
                  postLoadedAll: false,
                  showFilter: false,
                  categories: [],
                  industries: [],
                  posts: [],
                  filterqueryParam:{
                      category: 0,
                      filter: [],
                  },
                  paginationData: {
                      start: 0,
                      quantity: 10,
                  },
                  industryActive: [0,],
                  industryBase: '',
                  url:{
                      categories: "/api/categories",
                      industries: "/api/industries",
                      posts: "/api/posts",
                      fresh: "/api/fresh"
                  },
                  freshCounter: 0,
              }
          },
          created: function(){
              this.filterqueryParam.filter = this.industryActive;
              this.getAll();
          },
          mounted: function(){
              this.onScroll();
          },
          computed: {
              pageLoading: function(){
                  if(!this.bannerLoading && !this.baseLoading){
                      this.$emit('pageloaded');
                      return false
                  } else{
                      return true
                  }
              },
          },
          methods:{
              grabUrl: function(param){
                  var url_string = window.location.href;
                  var url = new URL(url_string);
                  var parametr = url.searchParams.get(param);
                  return parametr
              },
              setActive: function(id){
                  var isChildren = false
                  for (const item of this.categories){
                      for (const el of item.children){
                          if (id === el.id){
                              item.parent.isActive = false
                              item.parent.isSelected = true
                              el.isActive = true
                              this.filterqueryParam.category = id
                              isChildren = true
                          }
                          else {
                              el.isActive = false
                          }
                      }
                  }
                  if (isChildren){
                      return null
                  }


                  for (const item of this.categories){
                      item.parent.isSelected = false
                      if (item.children.length !== 0){
                          if (id === item.parent.id){
                              item.isDropdownexpanded = true
                          }
                          else{
                              item.isDropdownexpanded = false
                          }
                      }
                  }



                  for (const item of this.categories){
                      if (id === item.parent.id){
                          item.parent.isActive = true
                          this.filterqueryParam.category = id
                      }
                      else {
                          item.parent.isActive = false
                      }
                  }

              },
              hasChildren: function(item){
                  if(item.children.length !== 0){
                      return true
                  }
                  return false
              },
              categoryFormater: function(list){
                  var first = [
                      {
                          'parent':{
                              'id': 0,
                              'name': 'Свежее',
                              'isActive': false
                          },
                          'children':[]
                      }
                  ]



                  for (const item of list){
                      item.isActive = false
                      if (item.parent_id === null){
                          first.push(
                              {
                                  "isDropdownexpanded":false,
                                  "parent":item
                              }
                          )
                      }
                  }

                  for (const el of first){
                      el.children = []
                      for (const item of list){
                          if (item.parent_id === el.parent.id){
                              el.children.push(item)
                          }
                      }
                  }

                  return first
              },
              industryFormater: function(list){
                  var first ={
                      'id': 0,
                      'name':'Все'
                  }
                  list.unshift(first)
                  return list
              },
              getAll: function () {
                  this.baseLoading = true;

                  var requestCategories = axios.get(this.url.categories);
                  var requestIndustries = axios.get(this.url.industries);
                  var requestFresh = axios.get(this.url.fresh);

                  axios.all([requestCategories, requestIndustries,requestFresh ]).then(axios.spread((...responses) => {
                    var responseCategories = responses[0]
                    var responseIndustries = responses[1]
                    var responseFresh = responses[2]
                    this.categories = this.categoryFormater(responseCategories.data);
                    if (this.grabUrl("filter") === "quizz"){
                        this.setActive(1);
                    }
                    else{
                        this.setActive(0);
                    }
                    this.industries = this.industryFormater(responseIndustries.data);
                    this.freshCounter  = responseFresh.data
                    this.baseLoading = false;
                 })).catch(errors => {
                   // react on errors.
                 })
              },
              getPosts:  function(filterqueryParam){
                  axios({
                      method: 'get',
                      params: {
                                  category: filterqueryParam.category,
                                  filter: filterqueryParam.filter,
                                  start: this.paginationData.start,
                                  quantity: this.paginationData.quantity,
                              },
                      url: this.url.posts,
                  })
                  .then(response => {
                      this.posts = this.posts.concat(this.postFormater(response.data.posts))
                      this.postLoading = false
                      if (response.data.posts.length < this.paginationData.quantity){
                          this.postLoadedAll = true
                      }
                  })
              },
              onScroll:function(){
                  window.onscroll = () => {
                      var scrollHeight = Math.max(
                          document.body.scrollHeight, document.documentElement.scrollHeight,
                          document.body.offsetHeight, document.documentElement.offsetHeight,
                          document.body.clientHeight, document.documentElement.clientHeight
                      );
                      var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
                      let bottomOfWindow = scrollTop + window.innerHeight === scrollHeight;

                      if (bottomOfWindow && !this.postLoadedAll) {
                          this.postLoading = true
                          this.paginationData.start = this.paginationData.start + this.paginationData.quantity
                          this.getPosts(this.filterqueryParam)
                      }
                  }
              },
          },
          watch:{
              pageLoading:function(){
              },
              filterqueryParam:{
                 deep: true,
                 handler: _.debounce(function (newValue, oldValue) {
                     this.paginationData.start = 0
                     this.posts = []
                     this.postLoading = true
                     this.getPosts(newValue)
                 }, 400),
              },
              industryActive:_.debounce(function(newValue, oldValue){

                  if(newValue.includes(0) && !oldValue.includes(0)){
                      this.industryActive.splice(0)
                      this.industryActive.push(0)
                  }

                  if(newValue.length === (this.industries.length -1) && !newValue.includes(0)){
                      this.industryActive.splice(0)
                      this.industryActive.push(0)
                      this.filterqueryParam.filter = this.industryActive
                  }

                  if(newValue.length > 1 && oldValue.includes(0)){
                      this.industryActive.splice(0,1)
                      this.filterqueryParam.filter = this.industryActive
                  }

                  this.filterqueryParam.filter = this.industryActive
              }, 100),
          },
      })

    // ------ SEARCH COMPONENT
    Vue.component('search', {
        extends: baseMixin,
        delimiters: ["[[","]]"],
        template: '#search-template',
        props: ['show'],
        data: function () {
            return {
                postSearchLoading: false,
                postSearchLoadedAll: false,
                isFound: false,
                isSearchinit: true,
                paginationDatasearch: {
                    start: 0,
                    quantity: 10,
                },
                searchQuery: '',
                searchSort: '',
                searchResults: [],
                searchresultsCount: 0,
                url:{
                    posts: "/api/posts",
                },
                dropdownData: [
                    {
                        id: 0,
                        name: 'По релевантности',
                        isSelected: true,
                    },
                    {
                        id: 1,
                        name: 'По дате',
                        isSelected: false,
                    },
                ],
            }
        },
        created:function(){
            this.searchSort = this.dropdownData[0].id
        },
        mounted: function(){
            this.onScrollSearch();
        },
        computed: {

        },
        methods:{
            openSearch: function(){
               let self = this
               Vue.nextTick()
                   .then(function () {
                       self.$refs.search.focus()
                   })
            },
            closeSearch:function(){
                this.$emit('searchclose');
                this.searchQuery=""
                this.searchResults = []
                this.isFound = false
                this.isSearchinit = true
                this.paginationDatasearch.start = 0
            },
            getSearchqueries: function(query, sort){
                if (query.length  !== 0){
                    this.postSearchLoading = true
                    this.postSearchLoadedAll = false

                    this.searchResults = []
                    this.isFound = false
                    this.isSearchinit = true
                    this.paginationDatasearch.start = 0


                    axios({
                        method: 'get',
                        params: {
                                    query: query,
                                    sort: sort,
                                    start: this.paginationDatasearch.start,
                                    quantity: this.paginationDatasearch.quantity,
                                },
                        url: this.url.posts,
                    })
                    .then(response => {
                        this.searchResults = this.searchResults.concat(this.postFormater(response.data.posts))
                        this.searchresultsCount = (response.data.count)

                        this.isSearchinit = false
                        if (response.data.posts.length !== 0){
                            this.isFound = true
                        }
                        this.postSearchLoading = false
                        if (response.data.posts.length < this.paginationDatasearch.quantity){
                            this.postSearchLoadedAll = true
                        }
                    })
                } else{
                    this.isFound = false
                    this.isSearchinit = true
                    this.searchResults = []

                }
            },
            getSearchqueriesPaginator: function(query, sort){
                if (query.length  !== 0){
                    this.postSearchLoading = true
                    axios({
                        method: 'get',
                        params: {
                                    query: query,
                                    sort: sort,
                                    start: this.paginationDatasearch.start,
                                    quantity: this.paginationDatasearch.quantity,
                                },
                        url: this.url.posts,
                    })
                    .then(response => {
                        this.searchResults = this.searchResults.concat(this.postFormater(response.data.posts))
                        this.searchresultsCount = (response.data.count)
                        this.postSearchLoading = false
                        if (response.data.posts.length < this.paginationDatasearch.quantity){
                            this.postSearchLoadedAll = true

                        }
                    })
                }
            },
            onScrollSearch:function(){
                var searchEl = document.getElementById('search')
                searchEl.onscroll = () => {

                   var scrollHeight = searchEl.scrollHeight
                   var scrollTop = searchEl.scrollTop;
                   let bottomOfWindow = scrollTop + window.innerHeight === scrollHeight;
                   if (bottomOfWindow && !this.postSearchLoadedAll && this.isFound) {
                       this.postSearchLoading = true
                       this.paginationDatasearch.start = this.paginationDatasearch.start + this.paginationDatasearch.quantity
                       this.getSearchqueriesPaginator(this.searchQuery, this.searchSort)

                   }

                }
            },
        },
        watch:{
            show:function(newValue, oldValue){
                this.openSearch()
            },
            searchQuery: _.debounce(function(newValue, oldValue){
                this.getSearchqueries(newValue, this.searchSort)
            }, 400),
            searchSort: _.debounce(function(newValue, oldValue){
                this.getSearchqueries(this.searchQuery, newValue)
            }, 400),
        },
    })


    // ------ VUE MAIN
    var app = new Vue({
     el: '#app',
     delimiters: ["[[","]]"],
     data: {
         pageLoading: true,
         classBody:{},
         classNav:{},

         isAuth: false,

         showSearch: false,
         showAuth: false,
         showMenu: false,
         showSubscribe: false,

         postBodyData:null,
         postPageDefaultData:[
             {
                 id: 0,
                 order: 0,
                 typeId: 1,
                 content: {
                     headerimage: '/img/articles/demo.png',
                 },
             },
             {
                 id: 1,
                 order: 1,
                 typeId: 0,
                 content: {
                     text: '<p><strong>Lorem ipsum dolor</strong></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
                 },
             },
             {
                 id: 2,
                 order: 2,
                 typeId: 3,
                 content: {
                     text: 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?',
                 },
             },
             {
                 id: 3,
                 order: 3,
                 typeId: 3,
                 content: {
                     text: 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?',
                 },
             },
             {
                 id: 4,
                 order: 4,
                 typeId: 4,
                 content: {
                     text: 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?',
                 },
             },
             {
                 id: 5,
                 order: 5,
                 typeId: 4,
                 content: {
                     text: 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?',
                 },
             },
             {
                 id: 6,
                 order: 6,
                 typeId: 5,
                 content: {
                     text: '<iframe width="560" height="315" src="https://www.youtube.com/embed/6ardHZ-vO7s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
                 },
             },
         ],

     },
     created:function(){
         this.setColor();
         axios.defaults.xsrfCookieName = 'csrftoken';
         axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
         this.authChecker()
     },
     beforeMount: function() {
         try {
             var data = this.$el.querySelector('[name="body"]').value
             if(data.length >0) {
                  this.postBodyData = data
             } else{
                 this.postBodyData = JSON.stringify(this.postPageDefaultData)
             }
         }
         catch (e) {}
     },
     computed:{
         postBodyDataFormated:function(){
             var data ={
                 body: this.postBodyData,
                 title: 'Test title',
                 views: 1000,
                 likes: 1000,
                 industry:{
                     name: 'Test industry'
                 },
                 author_id:{
                     name: 'Test author'
                 }
             }
             return data
         },
     },
     methods:{
         authChecker:function(){
             this.isAuth = false
             localStorage.setItem('test', 1);
             console.log('this.isAuth',this.isAuth)
             console.log('this.isAuth',localStorage.getItem('test'))
         },
         setColor: function(){
             var url = (window.location.href).split("/")

             if (url.includes("quizz")){
                 this.classBody = {
                    body: false,
                    "body-white": true
                 }
                 this.classNav = {
                    "nav-project-gray": false,
                    "nav-project-white": true
                 }
             } else if (url.includes("posts")){

                 this.classBody = {
                    body: false,
                    "body-white": true
                 }
                 this.classNav = {
                    "nav-project-gray": false,
                    "nav-project-white": true
                 }
             } else{

                 this.classBody = {
                    body: true,
                    "body-white": false
                 }
                 this.classNav = {
                    "nav-project-gray": true,
                    "nav-project-white": false
                 }
             }
         },
         openSearch:function(){
             this.showSearch = true
             this.classBody.modalopen  = true
         },
         closeSearch:function(){
             this.showSearch = false
             this.classBody.modalopen  = false
         },
         openModal:function(id, event){
             if (id === "subscribe"){
                 this.showSubscribe = true
                 this.classBody.modalopen  = true
             }
             if (id === "postedit"){
                 this.showPostEdit = true
                 this.classBody.modalopen  = true
             }
             if (id === "auth"){
                 this.showAuth = true
                 this.classBody.modalopen  = true
             }

         },
         closeModal:function(id){
             if (id === "subscribe"){
                 this.showSubscribe = false
                 this.classBody.modalopen  = false
             }
             if (id === "postedit"){
                 this.showPostEdit = false
                 this.classBody.modalopen  = false
             }
             if (id === "auth"){
                 this.showAuth = false
                 this.classBody.modalopen  = false
             }
         },
         postPagechange:function(data){
             this.postBodyData = data
         },
     },

    })
});
