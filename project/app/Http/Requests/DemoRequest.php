<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DemoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
          'title' => 'required|string|unique:App\Demo,title',
          'description' => 'string',
          'location' => 'string',
          'image' => '',
          'complexity' => 'required|min:1|max:10',
          'isActive' => 'boolean'
        ];

        switch ($this->getMethod())
        {
            case 'POST':
                return $rules;
            case 'PUT':
                return [
                'title' => [
                    'required',
                    Rule::unique('App\Demo')->ignore($this->title, 'title') //должен быть уникальным, за исключением себя же
                ]
                ] + $rules; // и берем все остальные правила
            case 'DELETE':
                return [
                ];
        }
    }


    public function messages()
    {
        return [

        ];
    }

}
