<?php

namespace App\Http\Controllers;

use App\Category;

use Illuminate\Http\Request;
use App\Http\Requests\DemoRequest;


class MainController extends Controller
{
    public function index()
    {

        $categories  = Category::whereNull('parent_id')->orderBy('order', 'asc')->get();
        return view('mainpage.main', [
            'categories' => $categories,
            'ismain' => 'true',
        ]);

    }
}
