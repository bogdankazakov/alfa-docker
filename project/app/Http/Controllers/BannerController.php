<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Post;
use App\Quizz;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::where('isActive', 1)
            ->orderBy('order', 'asc')
            ->get();

        foreach ($banners as $banner){
            $related_item = null;
            if($banner->type === 'POST'){
                    try {
                        $id = $banner->related_item_id;
                        $related_item = Post::with('industry:id,name', 'category:id,name', 'authorId:id,name')
                        ->select('title','published_at','views','likes', 'industry_id', 'category_id')
                        ->withCount('comments')
                        ->find($id);
                    } catch (Exception $e) {

                }
            } elseif($banner->type === 'QUIZZ'){
                    try {
                        $id = $banner->related_item_id;
                        $related_item  = Quizz::with('questions.answers', 'industry:id,name')
                        ->find($id);
                    } catch (Exception $e) {

                }
            } else {
            }

            $banner->related_item = $related_item;
        }


        return [
            'banners' =>  $banners,
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        //
    }
}
