<?php

namespace App\Http\Controllers;

use App\Mail\AskQuestion;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Mail;

class AuthorExtraController extends Controller
{


    public function askquestion(Request $request)
    {

        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'message' => $request->input('message'),
        ];
        Mail::to('ir.alfastrah@gmail.com')->send(new AskQuestion($data));

        return response('', 200);
        // return (new App\Mail\AskQuestion($data))->render();

    }



}
