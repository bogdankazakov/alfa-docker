<?php

namespace App\Http\Controllers;
use App\Category;
use App\Quizz;
use Illuminate\Http\Request;

class QuizzController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('quizz.main');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $quizz  = Quizz::with('questions.answers', 'industry:id,name')->findOrFail($id);
        $categories  = Category::whereNull('parent_id')->orderBy('order', 'asc')->get();

        return view('quizz.main', [
            'quizz' => $quizz,
            'title' => empty($quizz['seo_title']) ? $quizz['title'] : $quizz['seo_title'],
            'keywords' => empty($quizz['meta_keywords']) ? null : $quizz['meta_keywords'],
            'description' => empty($quizz['meta_description']) ? null : $quizz['meta_description'],
            'image' => empty($quizz['image']) ? null : $quizz['image'],
            'categories' => $categories,

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
