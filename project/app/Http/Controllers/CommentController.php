<?php

namespace App\Http\Controllers;

use App\Comment;
use App\User;
use Illuminate\Http\Request;
use Storage;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sortby = $request->input('sortby');

        if($sortby === "time"){
            $comments = Comment::with('author:id,name,avatar,role_id')
                ->where('post_id', $request->input('post_id'))
                ->orderBy('created_at', 'desc')
                ->get();
        }
        if($sortby === "rate"){
            $comments = Comment::with('author:id,name,avatar,role_id')
                ->where('post_id', $request->input('post_id'))
                ->orderBy('likes', 'desc')
                ->get();
        }


        return [
            'comments' =>  $comments,
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comment = new Comment;
        $comment->body = $request->input('comment');
        $comment->post_id = $request->input('post_id');
        if($request->input('author')){
            if($request->input('author')['social'] === 'FB'){
                $r_user_id =  $request->input('author')['id'];
                $r_user_name = $request->input('author')['name'];
                $db_user = User::where('fb_id',$r_user_id)->first();
                if  ($db_user === null){
                    $author = new User;
                    $author->has_fb = true;
                    $author->fb_id = $r_user_id;
                    $author->name = $r_user_name;
                    $author->email = time();
                    $author->password = time();

                    $url = $request->input('author')['avatar'];
                    $file = file_get_contents($url);
                    $name = "users/avatars/".$request->input('author')['social'].'_'.$r_user_id.'.jpg';
                    $path = Storage::put("public/".$name, $file);
                    $author->avatar = $name;

                    $author->save();
                }else{
                    $author = $db_user;
                }
            };
        };
        $comment->author_id =  $author->id;
        $comment->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
