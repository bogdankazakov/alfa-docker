<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Comment;

class CommentExtraController extends Controller
{


    public function addcommentlike(Request $request)
    {

        $id = $request->input('id');
        $type = $request->input('type');

        $comment  = Comment::findOrFail($id);
        if($type === 'minus'){
            $new = $comment->likes - 1;
            $comment->likes = $new;
        }
        if($type === 'plus'){
            $new = $comment->likes + 1;
            $comment->likes = $new;
        }
        $comment->save();

        return [
            'likes' =>  $new,
                ];
    }



}
