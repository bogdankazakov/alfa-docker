<?php

namespace App\Http\Controllers;
use App\Category;
use App\Post;
use App\Quizz;
use App\Industry;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $posts =  Post::getActivePosts();
        $quizzs = Quizz::getActiveQuizzs();

        function objarray($obj)
        {
            $arr = collect();
            foreach ($obj as $key=>$value)
                $arr->push($value);
            return $arr;
        }

        function mergerNonrelevant($posts, $quizzs)
        {
            $postsAndquizzs=collect();
            foreach ($posts as $item)
                $postsAndquizzs->push($item);
            foreach ($quizzs as $item)
                $postsAndquizzs->push($item);
            $postsAndquizzs = $postsAndquizzs->sortByDesc('published_at');
            $postsAndquizzs = objarray($postsAndquizzs);
            return $postsAndquizzs;
        }
        function mergerRelevant($exactPosts, $exactQuizzs, $mergedPosts, $mergedQuizzs)
        {
            // сначала exact
            $postsAndquizzs_exact=collect();
            foreach ($exactPosts as $item)
                $postsAndquizzs_exact->push($item);
            foreach ($exactQuizzs as $item)
                $postsAndquizzs_exact->push($item);
            $postsAndquizzs_exact = $postsAndquizzs_exact->sortByDesc('published_at');

            // теперь merged
            $postsAndquizzs_merged=collect();
            foreach ($mergedPosts as $item){
                if($exactPosts->contains($item)){

                } else{
                    $postsAndquizzs_merged->push($item);
                }
            }
            foreach ($mergedQuizzs as $item){
                if($exactQuizzs->contains($item)){

                } else{
                    $postsAndquizzs_merged->push($item);
                }
            }



            $postsAndquizzs_merged = $postsAndquizzs_merged->sortByDesc('published_at');


            $postsAndquizzs=$postsAndquizzs_exact ->merge($postsAndquizzs_merged);
            $postsAndquizzs = objarray($postsAndquizzs);
            return $postsAndquizzs;
        }

        function queryprepare($query)
        {
            $query = trim($query);
            $query = preg_replace("/(?![.=$'€%-])\p{P}/u", '', $query);
            $list = explode(" ", $query);
            $query_list=collect();
            foreach ($list as $q) {
                if(mb_strlen($q, 'UTF-8') > 3){
                    $query_list->push($q);
                }
            }
            return $query_list;
        }

        if ($request->has('query')){
            if (mb_strlen($request->input('query'), 'UTF-8') > 3 ){
                $query = $request->input('query');
                $sort = $request->input('sort');
                $column = 'title';
                if($sort === "0"){
                    // по  релевантности
                    $exactPosts = Post::getActivePosts()->where($column, 'ILIKE', "%{$query}%")->get();
                    $exactQuizzs = Quizz::getActiveQuizzs()->where($column, 'ILIKE', "%{$query}%")->get();
                    $mergedPosts = Post::where('title', "xxx")->get();
                    $mergedQuizzs = Quizz::where('title', "xxx")->get();
                    $query_list = queryprepare($query);
                    foreach ($query_list as $q){
                        $pq = Post::getActivePosts()->where($column, 'ilike', "%{$q}%")->get();
                        $qq = Quizz::getActiveQuizzs()->where($column, 'ilike', "%{$q}%")->get();
                        $mergedPosts = $mergedPosts->merge($pq);
                        $mergedQuizzs = $mergedQuizzs->merge($qq);
                    }

                    $postsAndquizzs  = mergerRelevant($exactPosts, $exactQuizzs, $mergedPosts, $mergedQuizzs);
                }
                if ($sort === "1"){
                    // по  дате
                    // $posts = $posts->where($column, 'ilike', "%{$query}%")->get();
                    // $quizzs = $quizzs->where($column, 'ilike', "%{$query}%")->get();
                    // $postsAndquizzs  = merger($posts, $quizzs);
                    $mergedPosts = Post::where('title', "xxx")->get();
                    $mergedQuizzs = Quizz::where('title', "xxx")->get();
                    $query_list = queryprepare($query);
                    foreach ($query_list as $q){
                        $pq = Post::getActivePosts()->where($column, 'ilike', "%{$q}%")->get();
                        $qq = Quizz::getActiveQuizzs()->where($column, 'ilike', "%{$q}%")->get();
                        $mergedPosts = $mergedPosts->merge($pq);
                        $mergedQuizzs = $mergedQuizzs->merge($qq);
                    }

                    $postsAndquizzs  = mergerNonrelevant($mergedPosts, $mergedQuizzs);
                }
            } else{
                $mergedPosts = Post::where('title', "xxx")->get();
                $mergedQuizzs = Quizz::where('title', "xxx")->get();
                $postsAndquizzs  = mergerNonrelevant($mergedPosts, $mergedQuizzs);
            }

        }
        else {
            if ($request->category == 0) {
                $posts = $posts -> get();
                $quizzs = $quizzs -> get();
                $postsAndquizzs  = mergerNonrelevant($posts, $quizzs);
            } elseif ($request->category == 1) {
                $postsAndquizzs = $quizzs-> get();
            } else {
                $postsAndquizzs = $posts->where('category_id', $request->input('category'))-> get();
            }

            $filter  = $request->input('filter');
            if (in_array(0, $filter)) {
                $postsAndquizzs  = $postsAndquizzs;
            } else {
                $postsAndquizzs = $postsAndquizzs->filter(function ($value, $key)  use ($filter) {
                    return in_array($value['industry_id'], $filter);
                });
                $postsAndquizzs = objarray($postsAndquizzs);

            }
        }


        $start = $request->input('start');
        $quantity = $request->input('quantity');
        return [
            'count' =>  $postsAndquizzs->count(),
            'posts' => $postsAndquizzs->splice($start, $quantity),
                ];




        // $postsAndquizzs = $posts->merge($quizzs);
        //
        // if ($request->has('query')){
        //     $query = $request->input('query');
        //     $sort = $request->input('sort');
        //     $column = 'title';
        //     $posts = $posts->where('title', 'ilike', "%{$query}%");
        // }
        // else {
        //     if ($request->category == 0) {
        //         $posts = $posts;
        //     } else {
        //         $posts = $posts->where('category_id', $request->input('category'));
        //     }
        //
        //     if (in_array(0, $request->filter)) {
        //         $posts = $posts;
        //     } else {
        //         $posts = $posts->whereIn('industry_id',  $request->input('filter'));
        //     }
        // }
        //
        // $posts = $posts->get();


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::with('industry:id,name', 'category:id,name', 'authorId:id,name')->withCount('comments')->findOrFail($id);

        $body = $post->body;
        $post_render_ready = json_decode($body);

        usort($post_render_ready,function($first,$second){
            return $first->order > $second->order;
        });

        $categories  = Category::whereNull('parent_id')->orderBy('order', 'asc')->get();


        return view('posts.main', [
            'post' => $post,
            'post_render_ready' => $post_render_ready,
            'title' => empty($post['seo_title']) ? $post['title'] : $post['seo_title'],
            'keywords' => empty($post['meta_keywords']) ? null : $post['meta_keywords'],
            'description' => empty($post['meta_description']) ? null : $post['meta_description'],
            'image' => empty($post['image']) ? null : $post['image'],
            'categories' => $categories,

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
