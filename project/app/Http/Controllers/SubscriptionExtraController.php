<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Subscription;

use App\Exports\SubscriptionExport;
use Maatwebsite\Excel\Facades\Excel;

class SubscriptionExtraController extends Controller
{


    public function exportcomment(Request $request)
    {
        return Excel::download(new SubscriptionExport, 'subscriptions.xlsx');
    }



}
