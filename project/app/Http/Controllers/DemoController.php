<?php

namespace App\Http\Controllers;

use App\Demo;
use Illuminate\Http\Request;
use App\Http\Requests\DemoRequest;


class DemoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Demo::all();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DemoRequest $request)
    {
        $instance = Demo::create($request->validated());
        return $instance;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Demo  $demo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $instance = Demo::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Demo  $demo
     * @return \Illuminate\Http\Response
     */
    public function update(DemoRequest $request, $id)
    {

         $instance = Demo::findOrFail($id);
         $instance->fill($request->except(['demo_id']));
         $instance->save();
         return response()->json($instance);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Demo  $demo
     * @return \Illuminate\Http\Response
     */
    public function destroy(DemoRequest $request, $id)
    {
         $instance = Demo::findOrFail($id);
         if($instance->delete()) return response(null, 204);
    }
}
