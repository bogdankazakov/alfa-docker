<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demo extends Model
{
    protected $fillable = [
        'title',
        'location',
        'description',
        'image',
        'complexity',
        'isActive'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'];
}
