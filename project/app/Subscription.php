<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'isActive'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'];
}
