<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models\Post as VoyagerPost;

class Post extends VoyagerPost
{

    protected $fillable = [
        'published_at',
        'features',
        'features_icon',
    ];

    public function industry(){
        return $this->belongsTo('App\Industry')->where('isActive', 'true')->orderBy('order', 'asc');
    }

    public function category(){
        return $this->belongsTo('App\Category')->orderBy('order', 'asc');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public static function getActivePosts() {
    return self::with('industry:id,name', 'category:id,name', 'authorId:id,name')
        ->withCount('comments')
        ->where('status', 'PUBLISHED')
        ->orderBy('published_at', 'desc');
    }



}
