<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quizz extends Model
{
    protected $fillable = [
        'title',
        'order',
        'isActive',
        'published_at',
        'industry_id',
        'views',
        'likes',
        'seo_title',
        'meta_description',
        'meta_keywords',
        'image',
        'slug'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'];


    public function questions()
    {
        return $this->hasMany('App\Question');

    }

    public function industry(){
        return $this->belongsTo('App\Industry')->where('isActive', 'true')->orderBy('order', 'asc');
    }

    public function category(){
        return $this->belongsTo('App\Category')->orderBy('order', 'asc');
    }

    public static function getActiveQuizzs() {
    return self::with('industry:id,name', 'category:id,name')
        ->where('isActive', 1)
        ->orderBy('published_at', 'desc');
    }

    public function industryId(){
        return $this->belongsTo('App\Industry');
    }

}
