<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'name',
        'order',
        'isAnswer',
        'question_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'];
}
